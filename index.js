'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const apiai = require('apiai');
const http = require("http");
const fs = require("fs");
const path = require("path");
const mimeTypes = {
    '.html': 'text/html',
    '.css': 'text/css',
    '.jpg': 'image/jpeg'
}

const app = express();
const port = process.env.PORT || 3000;

const dialogflowApp = apiai("3b4f8c74fb544b5f9046a939db390ded");

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.post('/identificar', function (req, res) {
	console.log(req.body.mensaje);
	console.log(req.body);
	var request = dialogflowApp.textRequest(req.body.mensaje,{
		sessionId: '651561616'
	});

	request.on('response', function(response) {

	    console.log(response.result.metadata.intentName);
        console.log({mensaje: response.result.metadata.intentName});
	    res.status(200).send(response.result);
	});

	request.on('error', function(error) {
	    console.log(error);
	});

	request.end();
});

app.listen(port, () =>{
	console.log(`Api REST running on http://localhost:${port}`);
});

app.get('/',function (req, res) {
    req.url = "/cliente.html";
    retornarArchivo(req, res);
});


function retornarArchivo(request, response) {
    var ext = path.extname(request.url.substring(1));
    fs.readFile("." + request.url, recibirArchivo);

    function recibirArchivo(error, data) {
        if (error == null) {
            console.log("path " + ext);
            var fileType = mimeTypes[ext];
            console.log("tipo:" + fileType);
            response.writeHead(200, { 'content-type': fileType });
            response.write(data);
            response.end();
        } else {
            console.log(error);
            response.end(error.toString());
        }
    }
}
